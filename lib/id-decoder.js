const regexp = /^([12])([0-9]{2})([0-1][0-9])([0-9]{2}[AB0-9][0-9]{2})([0-9]{3})/;

const decodeGender = function(code) {
  if (code === '1') return 'male';
  if (code === '2') return 'female';
  return null;
};

const decodeCountry = function(code) {
  if (code.match(/^99[0-9]{3}/)) {
    return parseInt(code);
  }
  return null;
};

const decodeDepartment = function(code) {
  if (code.match(/^99/)) {
    return null;
  } else if (code.match(/^9[78][0-9]/)) {
    return code.match(/^([0-9]{3})/)[1];
  } else {
    return code.match(/^([0-9][0-9AB])/)[1];
  }
};

const decodeCity = function(code) {
  if (code.match(/^99/)) {
    return null;
  } else if (code.match(/^9[78][0-9][0-9]{2}/)) {
    return parseInt(code.match(/^[0-9]{3}([0-9]{2})/)[1]);
  } else {
    return parseInt(code.match(/^[0-9][0-9AB]([0-9]{3})/)[1]);
  }
};

const decode = function(code) {
  let match;
  if (!(code && (match = code.match(regexp)) != null)) {
    throw new Error('invalid syntax');
  }
  return {
    gender: decodeGender(match[1]),
    year: parseInt(match[2]),
    month: parseInt(match[3]),
    country: decodeCountry(match[4]),
    department: decodeDepartment(match[4]),
    city: decodeCity(match[4]),
    order: parseInt(match[5]),
  };
};

module.exports = decode;
module.exports.decodeGender = decodeGender;
