const expect = require('expect');

const decode = require('./id-decoder.js');

describe('social welfare id decoder', function() {

  describe('decode function', function() {

    it('should throw an error if the code is invalid', function() {
      expect(function() {decode(null);}).toThrow();
    });

    it('should decode a colombian code', function() {
      expect(decode('289029941905660')).toEqual({
        gender: 'female',
        year: 89,
        month: 2,
        country: 99419,
        department: null,
        city: null,
        order: 56,
      });
    });

    it('should decode a french code', function() {
      expect(decode('190017632904381')).toEqual({
        gender: 'male',
        year: 90,
        month: 1,
        country: null,
        department: '76',
        city: 329,
        order: 43,
      });
    });

  });

});
